import Vue from 'vue/dist/vue.js'

import sayOurName from './say_our_name'

const app = new Vue({
  el: '#app',
  data: {
    
    integrants: sayOurName('Genivaldo Silva, Jair Silva, Márcio Lôbo, Romário Almeida, Wanderson Luciano')
  },
  
})

