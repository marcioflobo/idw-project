import { equal } from 'assert'
import sayOurNames from '../src/say_our_name'

describe('sayOurNames test',() => {
  it('should return greet with with excitement',() => {
      equal(sayOurNames('test'), 'test!');
  });
});
